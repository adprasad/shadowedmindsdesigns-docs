#Privacy Policy

##Purpose

This privacy notice aims to give you information on how ShadowedMindsDesigns, Inc. (“ShadowedMindsDesigns”, “We”, “Us”)  collects and processes your personal data through your use of this application. If you have questions or concerns about the Privacy Policy, please contact Customer Care at [shadowedmindsdesigns@gmail.com](mailto:shadowedmindsdesigns@gmail.com).
It is important that you read this privacy notice together with any other privacy notice or fair processing notice we may provide on specific occasions when we are collecting or processing personal data about you so that you are fully aware of how and why we are using your data. This privacy notice supplements the other notices and is not intended to override them

###Third Party Links.
This application may include links to third-party websites, plug-ins and applications. Clicking on those links or enabling those connections may allow third parties to collect or share data about you. We do not control these third-party websites and are not responsible for their privacy statements. When you leave our website, we encourage you to read the privacy notice of every website you visit.

##What Information Do We Collect?

###Crash Data.
Application user may chose to provide application crash data. Data is used to analyze and resolve the cause of application instability.

##What Information Do We Use?
Data described above is used in the management and improvement of software provided. 

###GPS Data.
A user may choose to provide device GPS data to the application. This is used for looking up weather details related to the device's last known GPS location. Weather request does not differentiate requests for weather at user's current GPS location from general weather requests.

###Non-GPS Location Data.
Location data not provided from GPS functionality of device is either part of site management data or general weather lookup. The locations stored as part of use of WeatheredRoads is saved to the device.

###Consent to Voluntary Information Collection and Sharing
All the information users submit to ShadowedMindsDesigns is done on a voluntary basis. When a user click the application launch button on any of the applications developed by ShadowedMindsDesigns's they are indicating voluntarily consent to the conditions outlined in this policy. We do not collect personally identifiable information (e.g., name, address, phone number, e-mail address) unless you provide it. In all cases, the information collected is used to respond to user inquiries or to provide services requested by our users. Any information provided through one of our Web forms is removed from Web servers within seconds, increasing the protection for this information.

##How Do We Use the Information We Collect and Do We Share the Information We Receive?

Data collected is used to improve the function and quality of the application.

You may choose not to provide ShadowedMindsDesigns with Personally Identifiable Information or you may turn off by disabling related application permissions. If you make these decisions, you may continue to use the application with the remaining features. 

##We share customer information with third parties only as follows:

###Service Providers.
We may share your Personally Identifiable Information with companies that provide support and data services to us (such as a Google Places for “GPS to Common Name lookup” or Ad services). These companies are not authorized to use the information we share with them for any other purpose.
Other Transfers. We may share Personally Identifiable Information and other data with businesses controlling, controlled by, or under common control with ShadowedMindsDesigns. If ShadowedMindsDesigns is merged, acquired, or sold, or if some or all of our assets or equity are transferred, we may disclose or transfer Personally Identifiable Information and other data in connection with the associated transactions.

###Bankruptcy.
In the event of a ShadowedMindsDesigns bankruptcy, insolvency, reorganization, receivership, or assignment for the benefit of creditors, or the application of laws or equitable principles affecting creditors' rights generally, we may not be able to control how your personal information is treated, transferred, or used. If such an event occurs, your Personally Identifiable Information may be treated like any other ShadowedMindsDesigns asset and sold, transferred, or shared with third parties, or used in ways not contemplated or permitted under this Privacy Policy. In this case, you will be notified via email and/or a prominent notice on our site of any change in ownership or uses of your personal information, as well as any choices you may have regarding your personal information.

###Data Retention.
At minimum, we will retain your information for as long as needed to provide you services, and as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. ShadowedMindsDesigns may maintain some or all of this data in its archives even after it has been removed from the Site.

##How Secure Is the Personal Information We Collect?
Our Commitment to Data Security. We strive to make certain that our servers and connections incorporate the latest encryption and security devices. We have implemented physical, electronic, and managerial procedures to safeguard and secure the information we collect. Unfortunately, no data transmission is guaranteed to be 100% secure and we therefore cannot guarantee the security of information you transmit to or from the Site, Applications, or through the use of our services, and you provide this information at your own risk. ACCORDINGLY, WE DISCLAIM LIABILITY FOR THE THEFT, LOSS, OR INTERCEPTION OF, OR UNAUTHORIZED ACCESS OR DAMAGE TO, YOUR DATA OR COMMUNICATIONS BY USING THE SITE, APPLICATIONS, AND OUR SERVICES. YOU ACKNOWLEDGE THAT YOU UNDERSTAND AND ASSUME THESE RISKS.
IF YOU BELIEVE YOUR PRIVACY HAS BEEN BREACHED THROUGH USE OF OUR WEBSITE, APPLICATIONS, OR OTHER SERVICES PLEASE CONTACT US IMMEDIATELY AT [shadowedmindsdesigns@gmail.com](mailto:shadowedmindsdesigns@gmail.com).

###Enforcement. 
We periodically review this Privacy Policy and our compliance with it to verify that both are accurate. We encourage you to contact us with any concerns, and we will investigate and attempt to resolve any complaints and disputes about our privacy practices.

Changes to this Policy. We may update this privacy policy to reflect changes to our information practices. If we make any material changes a notice will be posted on this page along with the updated Privacy Policy prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.
Contact Us. If you have any questions regarding our Privacy Policy, please contact us at [shadowedmindsdesigns@gmail.com](mailto:shadowedmindsdesigns@gmail.com).
