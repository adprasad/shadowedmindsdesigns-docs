#Terms and Conditions

##Purpose

This terms and conditions statement aim to define expectations between you (application user) and ShadowedMindsDesigns, Inc. (“ShadowedMindsDesigns”, “Our”, “We”, “Us”). If you have questions or concerns about the 'Terms and Conditions', please contact Customer Care at [shadowedmindsdesigns@gmail.com](mailto:shadowedmindsdesigns@gmail.com).

This statement supplements the other notices and is not intended to override.

##Your Use of WeatheredRoads

You may use WeatheredRoads to browse, locate, view, or download Content for your mobile or other supported device ("Device"). To use WeatheredRoads, you will need a Device that meets the system and compatibility requirements for the relevant Content, working Internet access, and compatible software. The availability of Content and features will vary between countries and not all Content or features may be available in your country. Some Content may be available to share with family members. Content may be offered by ShadowedMindsDesigns or made available by third-parties not affiliated with ShadowedMindsDesigns.

##Availablity of WeatheredRoads

We reserve the right to unpublish the application features or entire application without notice.

##Links to Third-Party Websites

Our application may contain links to Websites owned or operated by parties other than ShadowedMindsDesigns, Inc. We do not monitor or control outside Websites and is not responsible for their content. Our inclusion of links to an outside Website does not imply any endorsement of the material on our Website or, unless expressly disclosed otherwise, any sponsorship, affiliation or association with its owner, operator or sponsor, nor does ShadowedMindsDesigns’ inclusion of the links imply that ShadowedMindsDesigns is authorized to use any trade name, trademark, logo, legal or official seal, or copyrighted symbol that may be reflected in the linked Website.

##Content Disclaimer

Weather, ad, geographic, and other data displayed on our application is provided by third-party vendors. We do not control the validity of external data and are not responsible their content. Updates and content changes on our application are at the discretion of the owning services. You should not assume that the information contained on our application has been updated or otherwise contains current information. ShadowedMindsDesigns does not review past data to determine whether they remain accurate and information contained in such old requests may have been superseded. THE INFORMATION AND MATERIALS IN OUR APPLICATION ARE PROVIDED FOR YOUR REVIEW IN ACCORDANCE WITH THE NOTICES, TERMS AND CONDITIONS SET FORTH HEREIN. THESE MATERIALS ARE NOT GUARANTEED OR REPRESENTED TO BE COMPLETE, CORRECT OR UP TO DATE. THESE MATERIALS MAY BE CHANGED FROM TIME TO TIME WITHOUT NOTICE.

##No Warranties; Exclusion of Liability; Indemnification

OUR APPLICATION IS OPERATED BY SHADOWEDMINDSDESIGNS ON AN "AS IS," "AS AVAILABLE" BASIS, WITHOUT REPRESENTATIONS OR WARRANTIES OF ANY KIND. SHADOWEDMINDSDESIGNS SHALL NOT HAVE ANY LIABILITY OR RESPONSIBILITY FOR ANY ERRORS OR OMISSIONS IN THE CONTENT OF OUR APPLICATION, FOR YOUR ACTION OR INACTION IN CONNECTION WITH OUR WEBSITE OR FOR ANY DAMAGE TO YOUR DEVICE OR DATA OR ANY OTHER DAMAGE YOU MAY INCUR IN CONNECTION WITH OUR APPLICATION. YOUR USE OF OUR APPLICATION ARE AT YOUR OWN RISK. IN NO EVENT SHALL EITHER SHADOWEDMINDSDESIGNS OR THEIR AGENTS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF OUR APPLICATION, THE DELAY OR INABILITY TO USE OUR APPLICATION OR OTHERWISE ARISING IN CONNECTION WITH OUR APPLICATION, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF ANY SUCH DAMAGES. IN NO EVENT SHALL SHADOWEDMINDSDESIGNS’ LIABILITY FOR ANY DAMAGE CLAIM EXCEED THE AMOUNT PAID BY YOU TO SHADOWEDMINDSDESIGNS FOR THE TRANSACTION GIVING RISE TO SUCH DAMAGE CLAIM.